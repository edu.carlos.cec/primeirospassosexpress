const express = require('express');
const app = express();


app.get("/", (req, res) => {
    res.send("Olá, mundo");
})
//get, post, ...

app.get("/sobre", (req, res) => {
    res.send("<h1> Aqui está o texto </h1>");
})


app.get("/contato", (req, res) => {
    res.sendFile(__dirname + "/index.html");
})

app.get("/confirmacao", (req, res) => {
    res.send("<h1> Conteudo enviado </h1>");
});


//Ultima coisa do arquivo
app.listen(3000, ()=>{
    console.log("O servidor está funcioando");
});

